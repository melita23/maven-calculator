package calc.calc;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class App extends Calculator implements ActionListener
{
	//static JFrame frame;
	static GridLayout grObj;
	static JLabel lblNum1, lblNum2, lblResult;
	static JTextField txtNum1, txtNum2, txtResult;
	static JButton btnAdd, btnSub, btnMul, btnDiv, btnClear, btnExit;
	static JPanel pnlNum1, pnlNum2, pnlResult, pnlCalc, pnlExtraBtns;
		
	public void create() {
		frame = new JFrame("Calculator");
		grObj = new GridLayout(5,1,10,10);
		
		lblNum1 = new JLabel("Number 1: ");
		lblNum2 = new JLabel("Number 2: ");
		lblResult = new JLabel("Result:       ");
		
		lblNum1.setFont(new java.awt.Font("Tahoma", 1, 30));
        lblNum1.setHorizontalAlignment(JTextField.CENTER);
        lblNum2.setFont(new java.awt.Font("Tahoma", 1, 30));
        lblNum2.setHorizontalAlignment(JTextField.CENTER);
        lblResult.setFont(new java.awt.Font("Tahoma", 1, 30));
        lblResult.setHorizontalAlignment(JTextField.CENTER);
		
		txtNum1 = new JTextField(10);
		txtNum2 = new JTextField(10);
		txtResult = new JTextField(10);
		
		txtNum1.setHorizontalAlignment(JTextField.CENTER);
        txtNum1.setFont(new java.awt.Font("Times New Roman", 1, 30));
        txtNum2.setHorizontalAlignment(JTextField.CENTER);
        txtNum2.setFont(new java.awt.Font("Times New Roman", 1, 30));
        txtResult.setHorizontalAlignment(JTextField.CENTER);
        txtResult.setFont(new java.awt.Font("Times New Roman", 1, 30));
		txtResult.setEditable(false);
		
		btnAdd = new JButton("Add");
		btnSub = new JButton("Subtract");
		btnMul = new JButton("Multiply");
		btnDiv = new JButton("Divide");
		btnClear = new JButton("Clear");
		btnExit = new JButton("Exit");
		
		btnAdd.setFont(new java.awt.Font("Tahoma", 1, 30));
		btnSub.setFont(new java.awt.Font("Tahoma", 1, 30));
		btnMul.setFont(new java.awt.Font("Tahoma", 1, 30));
		btnDiv.setFont(new java.awt.Font("Tahoma", 1, 30));
		btnClear.setFont(new java.awt.Font("Tahoma", 1, 30));
		btnExit.setFont(new java.awt.Font("Tahoma", 1, 30));
		
		pnlNum1 = new JPanel();
		pnlNum2 = new JPanel();
		pnlResult = new JPanel();
		pnlCalc = new JPanel();
		pnlExtraBtns = new JPanel();
		
		btnAdd.addActionListener(this);
		btnSub.addActionListener(this);
		btnMul.addActionListener(this);
		btnDiv.addActionListener(this);
		btnClear.addActionListener(this);
		btnExit.addActionListener(this);
	}
	
	public void display() {
		frame.setLayout(grObj);
		
		frame.add(pnlNum1);
		pnlNum1.add(lblNum1);
		pnlNum1.add(txtNum1);
		
		frame.add(pnlNum2);
		pnlNum2.add(lblNum2);
		pnlNum2.add(txtNum2);
		
		frame.add(pnlResult);
		pnlResult.add(lblResult);
		pnlResult.add(txtResult);
		
		frame.add(pnlCalc);
		pnlCalc.add(btnAdd);
		pnlCalc.add(btnSub);
		pnlCalc.add(btnMul);
		pnlCalc.add(btnDiv);
		
		frame.add(pnlExtraBtns);
		pnlExtraBtns.add(btnClear);
		pnlExtraBtns.add(btnExit);
		
		frame.setSize(500, 500);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String snum1 = txtNum1.getText();
		String snum2 = txtNum2.getText();
		JFrame myFrame = new JFrame();
		JLabel label = new JLabel("Enter only integers");
		label.setFont(new java.awt.Font("Tahoma", 1, 20));
        label.setHorizontalAlignment(JTextField.CENTER);
        JLabel label_div = new JLabel("Denominator cannot be zero!!");
		label_div.setFont(new java.awt.Font("Tahoma", 1, 20));
        label_div.setHorizontalAlignment(JTextField.CENTER);
		myFrame.setSize(100,100);
		
		if(e.getSource().equals(btnAdd)) {
			if(checkInt(snum1, snum2)) {
				int n1 = Integer.parseInt(snum1);
				int n2 = Integer.parseInt(snum2);
				txtResult.setText(Integer.toString(add(n1, n2)));
			} else {
				JOptionPane.showMessageDialog(myFrame, 
						  label, "Error", JOptionPane.ERROR_MESSAGE);
			}
			
		} else if(e.getSource().equals(btnSub)) {
			if(checkInt(snum1, snum2)) {
				int n1 = Integer.parseInt(snum1);
				int n2 = Integer.parseInt(snum2);
				txtResult.setText(Integer.toString(sub(n1, n2)));
			} else {
				JOptionPane.showMessageDialog(myFrame, 
						label, "Error", JOptionPane.ERROR_MESSAGE);
			}
			
		} else if(e.getSource().equals(btnMul)) {
			if(checkInt(snum1, snum2)) {
				int n1 = Integer.parseInt(snum1);
				int n2 = Integer.parseInt(snum2);
				txtResult.setText(Integer.toString(mul(n1, n2)));
			} else {
				JOptionPane.showMessageDialog(myFrame, 
						label, "Error", JOptionPane.ERROR_MESSAGE);
			}
			
		} else if(e.getSource().equals(btnDiv)) {
			if(checkInt(snum1, snum2)) {
				int n1 = Integer.parseInt(snum1);
				int n2 = Integer.parseInt(snum2);
				if(n2==0)
					JOptionPane.showMessageDialog(myFrame, 
						label_div, "Error", JOptionPane.ERROR_MESSAGE);
				else
					txtResult.setText(Integer.toString(div(n1, n2)));
			} else {
				JOptionPane.showMessageDialog(myFrame, 
						label, "Error", JOptionPane.ERROR_MESSAGE);
			}
			
		} else if(e.getSource().equals(btnClear)) {
			txtNum1.setText("");
			txtNum2.setText("");
			txtResult.setText("");
		} else if(e.getSource().equals(btnExit)) {
			System.out.println("Stop");
			System.exit(0);
		}
	}
	
    public static void main( String[] args )
    {
        System.out.println("Start");
        App app = new App();
        app.create();
        app.display();
    }
}