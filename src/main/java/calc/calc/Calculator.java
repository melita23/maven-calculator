package calc.calc;

import java.text.ParseException;

public class Calculator {
	
	public int add(int num1, int num2) {
		return (num1+num2);
	}
	
	public int sub(int num1, int num2) {
		return (num1-num2);
	}
	
	public int mul(int num1, int num2) {
		return (num1*num2);
	}
	
	public int div(int num1, int num2) {
		if(num2==0)
			throw new IllegalArgumentException("Number cannot be divided by 0!!");
		return (num1/num2);
	}
	
	public boolean checkInt(String snum1, String snum2) {
		try {
			int num1 = Integer.parseInt(snum1);
			int num2 = Integer.parseInt(snum2);
			return(true);
		} catch(NumberFormatException e) {
			return false;
		}
	}
}
