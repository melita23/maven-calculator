package calc.calc;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

public class AppTest {
    private Calculator app;
    
    @Before
    public void setup() {
        app = new Calculator();
    }
    
    @Test
    public void Add() {
    	assertEquals("Error in add()", 3, app.add(1, 2));
    	assertEquals("Error in add()", -3, app.add(-1, -2));
    	assertEquals("Error in add()", 9, app.add(9, 0));
    	assertEquals("Error in add()", 9, app.add(0, 9));
    	assertEquals("Error in add()", 0, app.add(0, 0));
    	assertEquals("Error in add()", 5, app.add(9, -4));
    	assertEquals("Error in add()", 5, app.add(-4, 9));
    }
    
    @Test
    public void Sub() {
    	assertEquals("Error in sub()", -1, app.sub(1, 2));
    	assertEquals("Error in sub()", 1, app.sub(-1, -2));
    	assertEquals("Error in sub()", 9, app.sub(9, 0));
    	assertEquals("Error in sub()", -9, app.sub(0, 9));
    	assertEquals("Error in sub()", 0, app.sub(0, 0));
    	assertEquals("Error in sub()", 13, app.sub(9, -4));
    	assertEquals("Error in sub()", -13, app.sub(-4, 9));
    }
    
    @Test
    public void Mul() {
    	assertEquals("Error in mul()", 2, app.mul(1, 2));
    	assertEquals("Error in mul()", 2, app.mul(-1, -2));
    	assertEquals("Error in mul()", 0, app.mul(9, 0));
    	assertEquals("Error in mul()", 0, app.mul(0, 9));
    	assertEquals("Error in mul()", 0, app.mul(0, 0));
    	assertEquals("Error in mul()", -36, app.mul(9, -4));
    	assertEquals("Error in mul()", -36, app.mul(-4, 9));
    }
    
    @Test
    public void Div() {
    	assertEquals("Error in div()", 0, app.div(1, 2));
    	assertEquals("Error in div()", 0, app.div(-1, -2));
    	assertEquals("Error in div()", 2, app.div(4, 2));
    	assertEquals("Error in div()", 2, app.div(-4, -2));
    	assertEquals("Error in div()", 0, app.div(0, 9));
    	assertEquals("Error in div()", -2, app.div(9, -4));
    	assertEquals("Error in div()", 0, app.div(-4, 9));
    }
}